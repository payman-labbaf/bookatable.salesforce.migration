﻿using System.Threading.Tasks;
using Salesforce.Force;

namespace Bookatable.Neptune.Admin.Services.Migration
{
    public class SalesforceMigration : ISalesforceMigration
    {
        private readonly IForceClient _client;
        private readonly IAccountPipe _pipe;

        public SalesforceMigration(IForceClient client, IAccountPipe pipe)
        {
            _client = client;
            _pipe = pipe;
        }

        public async Task Migrate(SalesforceAccountQuery query)
        {
            string nextRecordsUrl = null;
            do
            {
                var accounts = nextRecordsUrl == null
                    ? await _client.QueryAsync<Account>(query.ToString())
                    : await _client.QueryContinuationAsync<Account>(nextRecordsUrl);
                
                foreach (var account in accounts.Records)
                {
                    _pipe.Execute(account);
                }

                nextRecordsUrl = accounts.NextRecordsUrl;
            }
            while (!string.IsNullOrEmpty(nextRecordsUrl));
        }
    }
}
