namespace Bookatable.Neptune.Admin.Services.Migration
{
    public class SalesforceAccountQuery
    {
        private readonly string _lastSuccessfulCustomerId;
        private readonly int? _querySize;
        private readonly string _fields = "Account_Email__c, Booking__c, Culture__c, Customer_ID__c, ERB_Code__c, Facebook_Page__c, Fax, Id, IsSuspended__c, Market__c, Name, OmniaRestuarantID__c, Phone, ShippingCity, ShippingCountry, ShippingPostalCode, ShippingState, ShippingStreet, SignUpChannel__c, SignUpDate__c, Site, Test_Account__c, TimeZone__c, Twitter_Account__c, Type, Website, status__c, Customer_Start_Date__c, CreatedDate, MTP_Id__c, RepositoryProDatasheetID__c";

        public SalesforceAccountQuery(string lastSuccessfulCustomerId, int? querySize)
        {
            _lastSuccessfulCustomerId = lastSuccessfulCustomerId;
            _querySize = querySize;
        }

        public override string ToString()
        {
            return $"SELECT {_fields} FROM Account {WhereClause()} ORDER BY Customer_ID__c {LimitClause()}";
        }

        private string LimitClause()
        {
            return _querySize.HasValue
                ? $"LIMIT {_querySize}"
                : string.Empty;
        }

        private string WhereClause()
        {
            var whereClause = "WHERE Type NOT IN ('Live Partner', 'Group Prospect', 'Implementing Partner', 'Live Partner', 'Lost Partner', 'Prospect', 'Prospect Partner', 'Prospect Reseller', 'Reseller', 'Suspended Partner', 'Test & Demo', 'Self Sign Up Customer', 'Discarded Prospect', 'Lost Holding Company', 'Lost Brand', 'Holding Company', 'Brand')";
            whereClause = _lastSuccessfulCustomerId == null
                ? whereClause
                : $"{whereClause} AND Customer_ID__c > '{_lastSuccessfulCustomerId}'";

            return whereClause;
        }
    }
}