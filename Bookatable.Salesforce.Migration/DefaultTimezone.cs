namespace Bookatable.Neptune.Admin.Services.Migration
{
    public class DefaultTimezone : IAccountPipe
    {
        private readonly IAccountPipe _inner;

        public DefaultTimezone(IAccountPipe inner)
        {
            _inner = inner;
        }

        public Account Execute(Account account)
        {
            account = _inner.Execute(account);

            if (account.Timezone == null)
            {
                switch (account.Market)
                {
                    case "AUT":
                        account.Timezone = "Europe/Vienna";
                        break;
                    case "CHE":
                        account.Timezone = "Europe/Zurich";
                        break;
                    case "DAN":
                        account.Timezone = "Europe/Copenhagen";
                        break;
                    case "DEU":
                        account.Timezone = "Europe/Berlin";
                        break;
                    case "ESP":
                        account.Timezone = "Europe/Madrid";
                        break;
                    case "FIN":
                        account.Timezone = "Europe/Helsinki";
                        break;
                    case "FRA":
                        account.Timezone = "Europe/Paris";
                        break;
                    case "GBR":
                        account.Timezone = "Europe/London";
                        break;
                    case "ITA":
                        account.Timezone = "Europe/Rome";
                        break;
                    case "NLD":
                        account.Timezone = "Europe/Amsterdam";
                        break;
                    case "NOR":
                        account.Timezone = "Europe/Oslo";
                        break;
                    case "SWE":
                        account.Timezone = "Europe/Stockholm";
                        break;
                    case "USA":
                        account.Timezone = "US/Pacific";
                        break;
                    default:
                        account.Timezone = "Europe/London";
                        break;
                }
            }

            return account;
        }
    }
}