namespace Bookatable.Neptune.Admin.Services.Migration
{
    public class DefaultAddress : IAccountPipe
    {
        private const string UnavailableShippingDetail = "N/A";
        private readonly IAccountPipe _inner;

        public DefaultAddress(IAccountPipe inner)
        {
            _inner = inner;
        }

        public Account Execute(Account account)
        {
            account = _inner.Execute(account);
            
            account.State = account.State ?? UnavailableShippingDetail;
            account.Street = account.Street ?? UnavailableShippingDetail;
            account.City = account.City ?? UnavailableShippingDetail;
            account.Country = account.Country ?? UnavailableShippingDetail;
            account.PostCode = account.PostCode ?? UnavailableShippingDetail;

            return account;
        }
    }
}