using Amazon.SQS;
using Amazon.SQS.Model;
using Bookatable.Neptune.Common.Configuration;
using log4net;
using Newtonsoft.Json;

namespace Bookatable.Neptune.Admin.Services.Migration
{
    public class SendToSqs : IAccountPipe
    {
        private readonly IAccountPipe _inner;
        private readonly IAmazonSQS _sqsClient;
        private readonly ILog _log;
        private readonly string _queueUrl;

        public SendToSqs(IAccountPipe inner, IAmazonSQS sqsClient, IConfiguration config, ILog log)
        {
            _inner = inner;
            _sqsClient = sqsClient;
            _log = log;
            _queueUrl = config.AppSetting("salesforce.migration.queue.url");
        }

        public Account Execute(Account account)
        {
            account = _inner.Execute(account);

            var messageBody = JsonConvert.SerializeObject(account);

            var response = _sqsClient.SendMessage(new SendMessageRequest(_queueUrl, messageBody));
            _log.Debug($"Account with Salesforce Customer Id of '{account.SalesforceCustomerId}'  has been sent to SQS. Message Id {response.MessageId}.");

            return account;
        }
    }
}