namespace Bookatable.Neptune.Admin.Services.Migration
{
    public class DefaultCulture : IAccountPipe
    {
        private readonly IAccountPipe _inner;

        public DefaultCulture(IAccountPipe inner)
        {
            _inner = inner;
        }

        public Account Execute(Account account)
        {
            account = _inner.Execute(account);

            if (account.Culture == null)
            {
                switch (account.Market)
                {
                    case "AUT":
                        account.Culture = "de-AT";
                        break;
                    case "CHE":
                        account.Culture = "de-CH";
                        break;
                    case "DAN":
                        account.Culture = "da-DK";
                        break;
                    case "DEU":
                        account.Culture = "de-DE";
                        break;
                    case "ESP":
                        account.Culture = "es-ES";
                        break;
                    case "FIN":
                        account.Culture = "fi-FI";
                        break;
                    case "FRA":
                        account.Culture = "fr-FR";
                        break;
                    case "GBR":
                        account.Culture = "en-GB";
                        break;
                    case "ITA":
                        account.Culture = "it-IT";
                        break;
                    case "NLD":
                        account.Culture = "nl-NL";
                        break;
                    case "NOR":
                        account.Culture = "nb-NO";
                        break;
                    case "SWE":
                        account.Culture = "sv-SE";
                        break;
                    case "USA":
                        account.Culture = "en-US";
                        break;
                    default:
                        account.Culture = "en-GB";
                        break;
                }
            }

            return account;
        }
    }
}