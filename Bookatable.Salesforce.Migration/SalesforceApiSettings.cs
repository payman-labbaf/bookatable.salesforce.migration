namespace Bookatable.Neptune.Admin.Services.Migration
{
    public class SalesforceApiSettings
    {
        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string PasswordToken { get; set; }

        public string TokenUri { get; set; }
    }
}