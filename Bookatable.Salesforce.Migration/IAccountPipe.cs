namespace Bookatable.Neptune.Admin.Services.Migration
{
    public interface IAccountPipe
    {
        Account Execute(Account account);
    }
}