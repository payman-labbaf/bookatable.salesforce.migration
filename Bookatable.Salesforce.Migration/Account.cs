using Newtonsoft.Json;

namespace Bookatable.Neptune.Admin.Services.Migration
{
    public class Account
    {
        [JsonProperty(PropertyName = "Customer_ID__c")]
        public string SalesforceCustomerId { get; set; }

        [JsonProperty(PropertyName = "Id")]
        public string SalesforceInternalId { get; set; }

        [JsonProperty(PropertyName = "Test_Account__c")]
        public bool? IsTestRestaurant { get; set; }

        public string Name { get; set; }

        [JsonProperty(PropertyName = "ShippingState")]
        public string State { get; set; }

        [JsonProperty(PropertyName = "ShippingStreet")]
        public string Street { get; set; }

        [JsonProperty(PropertyName = "ShippingCity")]
        public string City { get; set; }

        [JsonProperty(PropertyName = "ShippingCountry")]
        public string Country { get; set; }

        public string Website { get; set; }

        public string Phone { get; set; }

        [JsonProperty(PropertyName = "ShippingPostalCode")]
        public string PostCode { get; set; }

        [JsonProperty(PropertyName = "Market__c")]
        public string Market { get; set; }

        [JsonProperty(PropertyName = "SignUpDate__c")]
        public string SignUpDate { get; set; }

        public string CreatedDate { get; set; }

        [JsonProperty(PropertyName = "Customer_Start_Date__c")]
        public string CustomerStartDate { get; set; }

        [JsonProperty(PropertyName = "RepositoryProDatasheetID__c")]
        public string DatasheetProId { get; set; }

        [JsonProperty(PropertyName = "MTP_Id__c")]
        public string MTPId { get; set; }

        [JsonProperty(PropertyName = "Culture__c")]
        public string Culture { get; set; }

        [JsonProperty(PropertyName = "TimeZone__c")]
        public string Timezone { get; set; }

        [JsonProperty(PropertyName = "SignUpChannel__c")]
        public string SignUpChannel { get; set; }

        [JsonProperty(PropertyName = "Account_Email__c")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "ERB_Code__c")]
        public string ERBCode { get; set; }

        [JsonProperty(PropertyName = "Facebook_Page__c")]
        public string FacebookPage { get; set; }

        public string Fax { get; set; }

        [JsonProperty(PropertyName = "IsSuspended__c")]
        public bool? IsSuspended { get; set; }

        [JsonProperty(PropertyName = "OmniaRestuarantID__c")]
        public string RestaurantId { get; set; }
    }
}