﻿using System.Threading.Tasks;

namespace Bookatable.Neptune.Admin.Services.Migration
{
    public interface ISalesforceMigration
    {
        Task Migrate(SalesforceAccountQuery query);
    }
}