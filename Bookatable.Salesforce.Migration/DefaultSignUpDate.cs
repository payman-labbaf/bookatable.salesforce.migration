namespace Bookatable.Neptune.Admin.Services.Migration
{
    public class DefaultSignUpDate : IAccountPipe
    {
        private readonly IAccountPipe _inner;

        public DefaultSignUpDate(IAccountPipe inner)
        {
            _inner = inner;
        }

        public Account Execute(Account account)
        {
            account = _inner.Execute(account);
            if (account.SignUpDate == null)
            {
                account.SignUpDate = account.CustomerStartDate ?? account.CreatedDate;
            }

            return account;
        }
    }
}