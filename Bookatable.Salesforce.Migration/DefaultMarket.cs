namespace Bookatable.Neptune.Admin.Services.Migration
{
    public class DefaultMarket : IAccountPipe
    {
        private readonly IAccountPipe _inner;

        public DefaultMarket(IAccountPipe inner)
        {
            _inner = inner;
        }

        public Account Execute(Account account)
        {
            account = _inner.Execute(account);
            if (account.Market == null)
            {
                account.Market = account.Country;
            }

            return account;
        }
    }
}