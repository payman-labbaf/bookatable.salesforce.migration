namespace Bookatable.Neptune.Admin.Services.Migration
{
    public class DefaultSignUpChannel : IAccountPipe
    {
        private readonly IAccountPipe _inner;

        public DefaultSignUpChannel(IAccountPipe inner)
        {
            _inner = inner;
        }

        public Account Execute(Account account)
        {
            account = _inner.Execute(account);
            if (account.SignUpChannel == null)
            {
                account.SignUpChannel = "salesforce";
            }

            return account;
        }
    }
}