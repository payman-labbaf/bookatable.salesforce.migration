﻿using System;
using System.Configuration;
using System.Net;
using Amazon;
using Amazon.SQS;
using Bookatable.Neptune.Admin.Services.Migration;
using log4net;
using Salesforce.Common;
using Salesforce.Force;

namespace Bookatable.Salesforce.Migration
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Starting to migrate SF account in {ConfigurationManager.AppSettings["Environment"]} with following parameters:");
            for(int i = 0; i<args.Length; i++)
            {
                Console.WriteLine($"arg{i}={args[i]}");
            }
            var lastSuccessfulCustomerId = args.Length > 0 ?  args[0] : null;
            int? querySize = args.Length > 1 ? ToNullableInt(args[1]) : null;
            
            var log = LogManager.GetLogger("Salesforce Migration");
            var config = new Neptune.Common.Configuration.Configuration();
            var sqsClient = new AmazonSQSClient(RegionEndpoint.EUWest1);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var auth = new AuthenticationClient();
            var settings = new SalesforceApiSettings
            {
                ClientId = ConfigurationManager.AppSettings["salesforce.client.id"],
                ClientSecret = ConfigurationManager.AppSettings["salesforce.client.secret"],
                Username = ConfigurationManager.AppSettings["salesforce.username"],
                Password = ConfigurationManager.AppSettings["salesforce.password"],
                PasswordToken = ConfigurationManager.AppSettings["salesforce.password.token"],
                TokenUri = ConfigurationManager.AppSettings["salesforce.token.uri"]
            };

            auth.UsernamePasswordAsync(
                settings.ClientId,
                settings.ClientSecret,
                settings.Username,
                settings.Password + settings.PasswordToken,
                settings.TokenUri).Wait();

            var _forceClient = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
            var pipe =
                new SendToSqs(
                    new DefaultAddress(new DefaultPhone(
                        new DefaultSignUpChannel(
                            new DefaultTimezone(
                                new DefaultCulture(new DefaultSignUpDate(new DefaultMarket(new NullPipe()))))))), sqsClient, config, log);
            SalesforceMigration sut = new SalesforceMigration(_forceClient, pipe);
            sut.Migrate(new SalesforceAccountQuery(lastSuccessfulCustomerId, querySize)).Wait();
            Console.WriteLine("Migration Done!");
        }

        public static int? ToNullableInt(string s)
        {
            int i;
            if (int.TryParse(s, out i)) return i;
            return null;
        }
    }
}
