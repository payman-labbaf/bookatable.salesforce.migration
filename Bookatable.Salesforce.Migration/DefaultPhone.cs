namespace Bookatable.Neptune.Admin.Services.Migration
{
    public class DefaultPhone : IAccountPipe
    {
        private const string DefaultPhoneNumber = "+++";
        private readonly IAccountPipe _inner;

        public DefaultPhone(IAccountPipe inner)
        {
            _inner = inner;
        }

        public Account Execute(Account account)
        {
            account = _inner.Execute(account);
            account.Phone = account.Phone ?? DefaultPhoneNumber;
            return account;
        }
    }
}